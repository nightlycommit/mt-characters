import {writeFileSync} from "fs";
import {createRequire} from "module";

const require = createRequire(import.meta.url);
const projectManifest = require("./package.json");

const productManifest = {
    main: 'index.js',
    types: 'index.d.ts',
    keywords: [
        'MT',
        'Standards',
        'Standards MT',
        'Swift'
    ],
    author: 'Eric MORAND <eric.morand@gmail.com>',
    license: 'MIT License',
    dependencies: projectManifest.dependencies
};

writeFileSync('dist/package.json', JSON.stringify(productManifest));