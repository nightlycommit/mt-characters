import * as tape from "tape";
import {YCharacterSet} from "../../src/character-sets/y.character-set";
import type {Test} from "tape";

tape('Y Character Set', ({test}) => {
    test('characters', ({same, end}) => {
        const characters = YCharacterSet.characters;

        same(characters, ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', ',', '-', '(', ')', '/', '=', '\'', '+', ':', '?', '!', '"', '%', '&', '*', '<', '>', ';', 'Space']);

        end();
    });

    test('validate', ({test}) => {
        const validate = YCharacterSet.validate;

        const testAString = (test: Test["test"], name: string, value: string, expectation: boolean) => {
            test(`${name}`, ({same, end}) => {
                const testCases: Array<{
                    prefix?: string;
                    suffix?: string;
                }> = [{}, {prefix: 'A'}, {suffix: 'A'}, {prefix: 'A', suffix: 'A'}];

                for (const {prefix, suffix} of testCases) {
                    const verb = expectation ? 'approve' : 'decline';

                    const sentence = prefix && suffix ?
                        `${name} surrounded by "${prefix}" and "${suffix}"` : (
                            prefix ? `${name} preceded by "${prefix}"` : (
                                suffix ? `${name} followed by "${suffix}"` : `${name} alone`
                            )
                        );

                    const title = `${verb} ${sentence}`;

                    same(validate(`${prefix || ''}${value}${suffix || ''}`), expectation, title);
                }

                end();
            });
        };

        testAString(test, 'Space', ' ', true);
        testAString(test, 'A 0 B 1', 'A 0 B 1', true);

        testAString(test, 'CrLf', '\r\n', false);
        testAString(test, 'AbCd', 'AbCd', false);
        testAString(test, 'Cr', '\r', false);
        testAString(test, 'Lf', '\r', false);
        testAString(test, 'é', 'é', false);
    });
});