import * as tape from "tape";
import {XCharacterSet} from "../../src/character-sets/x.character-set";
import type {Test} from "tape";

tape('X Character Set', ({test}) => {
    test('characters', ({same, end}) => {
        const characters = XCharacterSet.characters;

        same(characters, ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '/', '-', '?', ':', '(', ')', '.', ',', '\'', '+', 'CrLf', 'Space']);

        end();
    });

    test('validate', ({test}) => {
        const validate = XCharacterSet.validate;

        const testAString = (test: Test["test"], name: string, value: string, expectation: boolean) => {
            test(`${name}`, ({same, end}) => {
                const testCases: Array<{
                    prefix?: string;
                    suffix?: string;
                }> = [{}, {prefix: 'a'}, {suffix: 'a'}, {prefix: 'a', suffix: 'a'}];

                for (const {prefix, suffix} of testCases) {
                    const verb = expectation ? 'approve' : 'decline';

                    const sentence = prefix && suffix ?
                        `${name} surrounded by "${prefix}" and "${suffix}"` : (
                            prefix ? `${name} preceded by "${prefix}"` : (
                                suffix ? `${name} followed by "${suffix}"` : `${name} alone`
                            )
                        );

                    const title = `${verb} ${sentence}`;

                    same(validate(`${prefix || ''}${value}${suffix || ''}`), expectation, title);
                }

                end();
            });
        };

        testAString(test, 'Space', ' ', true);
        testAString(test, 'CrLf', '\r\n', true);
        testAString(test, 'AbCd', 'AbCd', true);

        testAString(test, 'Cr', '\r', false);
        testAString(test, 'Lf', '\r', false);
        testAString(test, 'é', 'é', false);
    });
});