import * as escapeStringRegexp from "escape-string-regexp";

export type CharacterSet = {
    readonly characters: Array<string>;
    validate: (value: string) => boolean;
};

export const createCharacterSet: (
    characters: CharacterSet["characters"]
) => CharacterSet = (characters) => {
    return {
        characters,
        validate: (value) => {
            const pattern = new RegExp(`^(${characters.map((character) => {
                if (character === 'CrLf') {
                    return '\r\n';
                }
                
                if (character === 'Cr') {
                    return '\r';
                }
                
                if (character === 'Lf') {
                    return '\n';
                }
                
                if (character === 'Space') {
                    return ' ';
                }

                return escapeStringRegexp(character);
            }).join('|')})*$`);

            return pattern.test(value);
        }
    }
}