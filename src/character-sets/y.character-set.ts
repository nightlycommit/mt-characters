import {createCharacterSet} from "../character-set";

/**
 * Other characters are not allowed (Error code M60).
 */
const specification = `A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 0 1 2 3 4 5 6 7 8 9 . , - ( ) / = ' + : ? ! " % & * < > ; Space`;

export const YCharacterSet = createCharacterSet(specification.split(' '));