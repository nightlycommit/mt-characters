import {createCharacterSet} from "../character-set";

/**
 * Some MT fields allow even a broader range of characters to provide additional information. When a "z" is indicated in the format of the field, the characters set is as follows:
 *
 * a b c d e f g h i j k l m n o p q r s t u v w x y z
 *
 * A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
 *
 * 0 1 2 3 4 5 6 7 8 9
 *
 * . , - ( ) / = ' + : ? ! " % & * < > ; {
 *
 * @ # _
 *
 * Cr Lf Space
 *
 * Other characters are not allowed, including the curly bracket '}' (Error code M60).
 */

/**
 * In a field format with several lines, the characters Cr and Lf must never be used as single characters and must only be used together in the sequence CrLf, that is, LfCr is not allowed. When CrLf is used in such fields, it will be interpreted as the end of one line of text and the start of the next line of text.
 */
const multipleLinesSpecification = `a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 0 1 2 3 4 5 6 7 8 9 . , - ( ) / = ' + : ? ! " % & * < > ; { @ # _ CrLf Space`;

export const MultipleLinesZCharacterSet = createCharacterSet(multipleLinesSpecification.split(' '));

/**
 * In all other fields, the characters Cr and Lf may be used as single characters or in sequence, such as, CrLf or LfCr.
 */
const singleLineSpecification = `a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 0 1 2 3 4 5 6 7 8 9 . , - ( ) / = ' + : ? ! " % & * < > ; { @ # _ Cr Lf Space`;

export const SingleLineZCharacterSet = createCharacterSet(singleLineSpecification.split(' '));
