import {createCharacterSet} from "../character-set";

/**
 * The characters Cr and Lf must never be used as single characters and must only be used together in the sequence CrLf, that is, LfCr is not allowed.
 *
 * When the character sequence CrLf is used in a field format with several lines, it is used to indicate the end of one line of text and the start of the next line of text.
 */
const specification = `a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 0 1 2 3 4 5 6 7 8 9 / - ? : ( ) . , ' + CrLf Space`;

export const XCharacterSet = createCharacterSet(specification.split(' '));


